import 'package:flutter/material.dart';
import 'package:fooderlich/theme/FooderlichTheme.dart';

import 'home.dart';

void main() {
  runApp(Fooderlich());
}

class Fooderlich extends StatelessWidget {
  final theme = FooderlichTheme.dark();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: theme,
      title: 'Fooderlich',
      home: Home()
    );
  }
}
